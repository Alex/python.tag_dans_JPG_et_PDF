from flask import Flask, render_template, request, redirect, url_for, jsonify, flash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
import json
from main import main, traitement

app = Flask(__name__)
app.secret_key = 'your_secret_alex-design_key-2024'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

# Utilisateurs fictifs pour l'exemple
users = {'AlexDesign': {'password': 'gb7b1-S7ECGxlcZMi5cnT'}}

class User(UserMixin):
    def __init__(self, username):
        self.id = username

@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username in users and users[username]['password'] == password:
            user = User(username)
            login_user(user)
            return redirect(url_for('index'))
        else:
            flash('Invalid username or password')
    return render_template('login.html')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/')
@login_required
def index():
    return render_template('index.html')

@app.route('/run', methods=['POST'])
@login_required
def run():
    filigramme = request.form['filigramme']
    result = traitement(filigramme) # on passe l'argument a la function 'main' du programme
    return jsonify(result)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

