# TAG files
Ce script permet d'ajouter un filigrame sur des images.

Ce script Python prend en entrée un répertoire contenant des fichiers image et des PDF.

- Pour chaque PDF trouvé, il extrait les pages sous forme d'images dans un répertoire temporaire.
- Il parcourt toutes les images du répertoire source (défini dans le fichier config.json) et leur ajoute
  un filigrame en travers.
- Chaque imags est ensuite sauvegardée dans un répertoire contenant le nom du filigramme (paramètre text
  dans le fichier de configuration config.json)

# Utilisation
Pour utiliser ce script, il faut:
- Installer le langage Python,
- Installer un environnement virtuel,
- Editer la configuration,
- L'executer :-)

## Installation de python
Télécharger et installer la dernière version de Python (3.12.2) à partir du site officiel : https://www.python.org/downloads/

Installer certaines librairies nécessire à Pillow:
```
sudo apt install python3-dev python3-pip libjpeg-dev zlib1g-dev
```


## Installation de l'environnement virtuel
Installer l'outil virtualenv (venv):
```pip install virtualenv```

Mise en place de l'environnement virtuel
```python -m venv .venv```

Activation de l'environnement virtuel (window):
```.venv\Scripts\activate```

Activation de l'environnement virtuel (mac):
```source .venv/bin/activate```

Installation des dépendences python:
```pip install -r requirements.txt```


## Configuration
Toute la configuration (le filigrame et le chemin du répertoire contenant les documents) est à faire dans le
fichier config.json.
- filigrame : le nom qui va apparaitre après la phrase : "Document exclusivement destiné à " (cette phrase n'est pas modifiable)
- source_directory : le chemin du répertoire contenant les documents dans lesquels il faut ajouter le filigrame. 
Il est conseillé de mettre un chemin absolue. 
- zipfile: Ce paramètre (optionnel) permet de sauvegarder le résultat dans un fichier zip. Pour que la sauvegarde se fasse il faut donner définir un nom de fichier avec son chemin complet.


## Execution
Une fois configuré, le script peut être executé.
L'execution doit se faire dans l'environnement virtuel, soit en ligne de commande, soit en utilisant le script de lancement.

Ligne de commande :```python main.py```

Script de lancement (Windows): double-cliquer sur le fichier ```tag Files.bat```

Script de lancement (Mac): double-cliquer sur le fichier ```tag Files.sh```
